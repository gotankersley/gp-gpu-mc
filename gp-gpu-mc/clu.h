#pragma once
//OpenCL Utility lib
#include <iostream>
#include <fstream>
#include <string>
#include <CL/cl.hpp>

std::vector<cl::Platform> allPlatforms;
std::vector<cl::Device> allDevices;

bool initCL(cl::Device& device, cl::Context& context, int deviceType) {
	//Platform
    cl::Platform::get(&allPlatforms);
    if (allPlatforms.size() == 0) {
        printf("No platforms found. Check OpenCL installation!\n");
        return false;
    }
    cl::Platform defaultPlatform = allPlatforms[0]; 
    printf("Using platform: %s\n", defaultPlatform.getInfo<CL_PLATFORM_NAME>().c_str());

	//Device	    
	defaultPlatform.getDevices(deviceType, &allDevices);
    if (allDevices.size() == 0) {
        printf("No devices found. Check OpenCL installation!\n");
        return false;
    }
    device=allDevices[0];
    printf("Using device: %s\n", device.getInfo<CL_DEVICE_NAME>().c_str());
	context = cl::Context(device);

	return true;
}

bool loadKernel(std::string fileName, std::string fnName, cl::Context context, cl::Device device, cl::CommandQueue& queue, cl::Kernel& kernel) {
	//Load from file	
	std::ifstream fin(fileName, std::ios::in);
	if(!fin.is_open()) {
		printf("Error reading: %s\n", fileName.c_str());
		return false;
	}

    std::string line = "";
	std::string kernelCode = "";
    while(std::getline(fin, line)) {
        kernelCode += "\n" + line;
	}
    fin.close();
	
	//Build the source
	cl::Program::Sources sources;	
	sources.push_back(std::make_pair(kernelCode.c_str(), kernelCode.length()));
	cl::Program program(context, sources);
    if(program.build(allDevices) != CL_SUCCESS){
        printf("Error building: %s\n", program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device).c_str());
        return false;
    }

	//Create queue
	queue = cl::CommandQueue(context, device);	
	cl_int err;
	kernel = cl::Kernel(program, fnName.c_str(), &err);	
	if (err != CL_SUCCESS) {
		printf("Error creating kernel: %s\n", fnName.c_str());
		return false;
	}
	return true;
}
