/* BOARD LAYOUT
Bit positions:
0 | 1 | 2   ||  9 |10 |11           Quads:
7 | 8 | 3   ||  16|17 |12           Q0 | Q1
6 | 5 | 4   ||  15|14 |13           --------
_________________________           Q2 | Q3
-------------------------
18|19 |20   ||  27|28 |29
25|26 |21   ||  34|35 |30
24|23 |22   ||  33|32 |31
Note that the bits are arranged like this to rotate the quads with bitwise rotations

TERMINOLOGY:
Board: The entire 6x6 playing board with 36 spaces
Quad: One of the 3x3 sub-boards
Units: [start range : end range]
- row,col: used with 2D board array, useful for display  [0 : 6]
- pos: position of bitboard array [0 : 36]
- mpos: a bitboard mask position. i.e mpos = (1 << pos). [0 : 2^36]
- x,y: screen coordinates measured in pixels.  [0 ~ 1024px]
Pin: The player's piece
Space: One of 36 spaces a player can place a pin 
*/
#pragma once
#include <intrin.h>
#include <string>
#include <vector>
#include <map>

//Typedefs
typedef unsigned __int64 uint64;
typedef unsigned long ulong; //Used by intrinsics
typedef std::vector<int> list;
typedef std::map<std::string,bool> hashMap;

//Constants
const int BOARD_SPACES = 36;
const int BOARD_QUADS = 4;
const int NUM_TO_WIN = 5;
const int ROW_SPACES = 6;
const int COL_SPACES = 6;
const int QUAD_SPACES = 9;
const int QUAD_ROW_SPACES = 3;
const int QUAD_COL_SPACES = 3;
const int QUAD_HALF_COUNT = 2;
const int ALL_ROTATIONS = 8;
const int INVALID = -1;

//Enums
enum {PLAYER1, PLAYER2};
enum {ROT_CLOCKWISE, ROT_ANTICLOCKWISE};

//Index maps
const int ROW[] = {0,0,0,1,2,2,2,1,1,0,0,0,1,2,2,2,1,1,3,3,3,4,5,5,5,4,4,3,3,3,4,5,5,5,4,4};
const int COL[] = {0,1,2,2,2,1,0,0,1,3,4,5,5,5,4,3,3,4,0,1,2,2,2,1,0,0,1,3,4,5,5,5,4,3,3,4};
const int POS[ROW_SPACES][COL_SPACES] = {
	{0,1,2,9,10,11},        //Row 0
	{7,8,3,16,17,12},       //Row 1
	{6,5,4,15,14,13},       //Row 2
	{18,19,20,27,28,29},    //Row 3
	{25,26,21,34,35,30},    //Row 4
	{24,23,22,33,32,31}     //Row 5
};

//Masks
const uint64 FULL = 0xfffffffff;
const uint64 INITIAL = 0x0;
const uint64 QUADS[] = {0xff,0x1fe00,0x3fc0000,0x7f8000000}; //Doesn't include centers
const uint64 WINS[] = {
	0x607, 0xe06, 0x30188, 0x31108, 0xc070, 0xe030, 0x181c0000, 0x38180000, 0xc06200000, 0xc44200000, 0x301c00000, 0x380c00000, //Horizontal
    0x20400c1, 0x30400c0, 0x4080122, 0x4880120, 0x30001c, 0x700018, 0x408018200, 0x608018000, 0x810024400, 0x910024000, 0x60003800, 0xe0003000, //Vertical    
    0x5000800a, 0x808000111, 0x888000110, 0x5001000a0, //Diagonal top-left to bottom-right
    0x2090410, 0x4128800, 0x5128000, 0x8a05000 //Diagonal top-right to bottom-left
};

//Macros
#define bitCount(board) __popcnt64(board)
inline uint64 POS_TO_MPOS(int pos) { 
	return 1I64 << pos; 
}
inline int MPOS_TO_POS(uint64 mpos) {
	return floor((log((float)mpos) * 1.44269504088896340736) + 0.5);
}
inline uint64 RotL(uint64 num, uint64 pos) {
	return ((num << pos) % 0x100) | (num >> (8 - pos));
}
inline uint64 RotR(uint64 num, uint64 pos) {
	return (num >> pos) | ((num << (8 - pos)) % 0x100);
}
inline uint64 Not(uint64 x) {
	return (~x) & FULL;	                
}
inline list bitScan(uint64 board) {
	list bits;
	ulong pos;
	while (_BitScanForward64(&pos, board)) {
		bits.push_back(pos);
		board ^= POS_TO_MPOS(pos);
	}
	return bits;
}
uint64 randSeed = 42;
inline uint64 random () {
	randSeed = (randSeed * 0x5DEECE66DL + 0xBL) & ((1L << 48) - 1);
	return randSeed >> 16;
}

//Board object
struct Board {
	uint64 p1;
	uint64 p2;
	bool turn;

	Board() {
		p1 = INITIAL;
		p2 = INITIAL;
		turn = PLAYER1;
	}

	bool placePin(int pos) {
		if (pos < 0 || pos >= BOARD_SPACES) return false;
		uint64 avail = Not(p1 | p2);
		uint64 mpos = POS_TO_MPOS(pos);
		if (avail & mpos) {                
			if (turn == PLAYER1) p1 = p1 ^ mpos;        
			else p2 = p2 ^ mpos; //Player 2				
			return true;
		}
		else return false;
	}

	void rotate(int quad, int rot) {
		p1 = rotateQuad(p1, quad, rot);
		p2 = rotateQuad(p2, quad, rot);	
		turn = !turn;    
	}

	uint64 rotateQuad(uint64 board, int quad, int rot) {      
		//Rot can be simplified - in situ
		//Extract quad from board  
		uint64 quadUnshifted = board & QUADS[quad];
		uint64 quadBoard = quadUnshifted >> (quad * QUAD_SPACES); 
    
		//Bitwise rotate, 3 places will rotate 90 degrees - note bitwise rot is opposite direction of visual
		uint64 rotQuad = (rot == ROT_CLOCKWISE)? RotL(quadBoard, QUAD_HALF_COUNT) : RotR(quadBoard, QUAD_HALF_COUNT);    
	
		//Add the rotated quad back to the board
		uint64 quadShifted = rotQuad << (quad * QUAD_SPACES);
		board = board & Not(QUADS[quad]); //Empty quad    
		uint64 rotBoard = board ^ quadShifted;
		return rotBoard;    
	}

	void makeRandomMove() {
		//Place pin in random open space
		uint64 avail = Not(p1 | p2);
		list availBits = bitScan(avail);
		int randPos = availBits[random() % availBits.size()];
		if (turn == PLAYER1) p1 = p1 ^ POS_TO_MPOS(randPos);
		else p2 = p2 ^ POS_TO_MPOS(randPos);
   
		//Rotate
		int randQuad = random() % BOARD_QUADS;
		int randRot = random() % 2;
		rotate(randQuad, randRot);	
	}

	std::vector<Board> getAllMoves() {
		uint64 avail = Not(p1 | p2);
		list availBits = bitScan(avail);
		std::vector<Board> moves;
		hashMap movesSet;
				
		for (int a = 0; a < availBits.size(); a++) {
			for (int i = 0; i < ALL_ROTATIONS; i++) {
				int q = i/2;
				int r = i%2;			
				Board newBoard = clone();
				if (turn == PLAYER1) newBoard.p1 = newBoard.p1 ^ POS_TO_MPOS(availBits[a]); //Place pin
				else newBoard.p2 = newBoard.p2 ^ POS_TO_MPOS(availBits[a]); //Place pin
				newBoard.rotate(q, r); //Rotate
				char buffer[30];
				//Ignore symmetrical moves
				sprintf(buffer, "%I64X_%I64X", newBoard.p1, newBoard.p2);
				std::string key = buffer;
				if (!movesSet.count(key)) {									
					movesSet[key] = true;
					moves.push_back(newBoard);
				}
			}
		}
		return moves;
	}	

	int simulate() { 
		bool curPlayer = turn;
		int moveCount = bitCount(p1 | p2);

		for (int i = 0; i < (BOARD_SPACES - moveCount); i++) {
			makeRandomMove(); //Changes turn
			if (isWin()) {
				if (turn != curPlayer) return 1;
				else return -1;
			}
		}
		return 0;
	}

	int isWin() {    
		//Check if there are enough pins on the board for a win   
		uint64 board = (turn == PLAYER1)? p1 : p2;
		int count = bitCount(board);
		if (count < NUM_TO_WIN) return false;     

		for (int i = 0; i < 32; i++) { //There are 32 possible winning lines of 5
			uint64 win = WINS[i];
			if ((board & win) == win) return true;			
		}
		return false;
	}
	
	bool deriveMove(Board after, int& pos, int& quad, int& rot) {
		//Derive the move (i.e. pin position and quad rotation) that was made by looking at the difference 
		//between a board state before and after the move was made. 
	
		//Note: there can be multiple moves that result in the same after state (e.g. rotating quad centers), 
		//so this just picks the first one it finds
		pos = INVALID;
		quad = INVALID;
		rot = INVALID;

		uint64 beforeBoard;
		uint64 beforeOpp;
		uint64 afterBoard;
		uint64 afterOpp;
		if (turn == PLAYER1) {
			beforeBoard = p1;
			beforeOpp = p2;
			afterBoard = after.p1;
			afterOpp = after.p2;
		}
		else {
			beforeBoard = p2;
			beforeOpp = p1;
			afterBoard = after.p2;
			afterOpp = after.p1;    
		}
		int afterCount = bitCount(afterBoard);	
	
		//Try all 8 possible quad rotations to look for one that is only one bit different after rotation
		for (int i = 0; i < ALL_ROTATIONS; i++) {
			int q = i/2;
			int r = i % 2;
			uint64 rotatedBoard = rotateQuad(beforeBoard, q, r);
			uint64 rotatedOpp = rotateQuad(beforeOpp, q, r);
			uint64 combinedBoard = rotatedBoard & afterBoard;		
			if (afterCount - bitCount(combinedBoard) == 1 && (rotatedOpp == afterOpp)) {
				uint64 reverseRotated = rotateQuad(afterBoard, q, !r); //Rotate after board in reverse to get position
				pos = MPOS_TO_POS(beforeBoard ^ reverseRotated);
				quad = q;
				rot = r;
				return true;
			}
		}
	
		//See if there is a move with no rotation (win)
		uint64 combinedBoard = beforeBoard & afterBoard;    
		if (afterCount - bitCount(combinedBoard) == 1 && (beforeOpp == afterOpp)) {
			pos = MPOS_TO_POS(beforeBoard ^ afterBoard);
			return true;						
		}
		return false;
	}

	Board clone() {
		Board newBoard;
		newBoard.p1 = p1;
		newBoard.p2 = p2;
		newBoard.turn = turn;
		return newBoard;
	}

	void print() {  				
		printf("0x%I64X, 0x%I64X, %i\n\n", p1, p2, turn);
		for (int r = 0; r < ROW_SPACES; r++) {
			if (r == 3) printf("-------\t ------------------\n");
			for (int c = 0; c < COL_SPACES; c++) {            
				if (c == 3) printf("|");
				int pos = POS[r][c];     
				uint64 mpos = POS_TO_MPOS(pos);            
				char space = ':';
				if (p1 & mpos) space = 'X';
				else if (p2 & mpos) space = 'O';
                printf("%c", space);				
			}
			printf("\t");
			for (int c = 0; c < COL_SPACES; c++) {
				if (c == 3) printf("|");
				printf(" %02d", POS[r][c]);
			}
			printf("\n");       
		}		
	}
};