#include <iostream>
#include <ctime>
#include "board.h"
#include "clu.h"

const int MC_SIMULATIONS = 100000;
const int MC_INFINITY = 1000000;


Board getMoveMonteCarlo(Board board) {
	int bestScore = -MC_INFINITY;
	int bestMove = INVALID;
	std::vector<Board> moves = board.getAllMoves();

	#pragma omp parallel for
	for (int m = 0; m < moves.size(); m++) {
		int score = 0;
		
		for (int s = 0; s < MC_SIMULATIONS; s++) {
			Board b = moves[m].clone();
			score += b.simulate();
		}

		if (score > bestScore) {
			bestScore = score;
			bestMove = m;
		}
		//Output score map
		printf("%i - %i\n", m, score);
	}
		
	return moves[bestMove];
}

                                                                               
void main() {	
	/*
	//srand((unsigned int) time(0));
	clock_t startTime = clock();	
	Board board;	


	//Init OpenCL
	cl::Device device;
	cl::Context context;	
	initCL(device, context, CL_DEVICE_TYPE_CPU); //CL_DEVICE_TYPE_ALL

	//Arg data
	std::vector<Board> moves = board.getAllMoves();
	int moveCount = moves.size();
	int seed = 42;//rand();	
		
	uint64* board_p1s = new uint64[moveCount];
	uint64* board_p2s = new uint64[moveCount];
	
	for (int i = 0; i < moveCount; i++) {
		board_p1s[i] = moves[i].p1;
		board_p2s[i] = moves[i].p2;		
	}
	int* scores = new int[moveCount];

	//Setup and run kernel	
	cl::CommandQueue queue;
	cl::Kernel kernel;
	loadKernel("board.cl", "evalMove", context, device, queue, kernel);		
	    
    cl::Buffer buffer_p1s(context, CL_MEM_READ_WRITE, sizeof(uint64) * moveCount);
	cl::Buffer buffer_p2s(context, CL_MEM_READ_WRITE, sizeof(uint64) * moveCount);
    cl::Buffer buffer_scores(context, CL_MEM_READ_WRITE, sizeof(int) * moveCount);	

	queue.enqueueWriteBuffer(buffer_p1s, CL_TRUE, 0, sizeof(uint64) * moveCount, board_p1s);
	queue.enqueueWriteBuffer(buffer_p2s, CL_TRUE, 0, sizeof(uint64) * moveCount, board_p2s);
		
	cl::Event event;
	
	int turn = board.turn;
	kernel.setArg(0, buffer_p1s);
	kernel.setArg(1, buffer_p2s);	
	kernel.setArg(2, turn);		
	kernel.setArg(3, seed);		
    kernel.setArg(4, buffer_scores);
	cl_int err = queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(moveCount), cl::NullRange, NULL, &event);
	if (err != CL_SUCCESS) {
		printf("Error - CommandQueue::enqueueNDRangeKernel()\n");
		exit(0);
	}
	queue.finish();	
	
	queue.enqueueReadBuffer(buffer_scores, CL_TRUE, 0, sizeof(int) * moveCount, scores);

	std::cout << "Moves: " << moveCount << std::endl;
	std::cout << "Scores: \n";		
	int bestScore = -MC_INFINITY;
	int bestMove = -1;
    for (int i = 0; i < moveCount; i++) {
		if (scores[i] > bestScore) {
			bestScore = scores[i];
			bestMove = i;
		}
		std::cout << scores[i] << std::endl;
    }
		
	std::cout << "Best score: " << bestScore << std::endl;
	moves[bestMove].print();

	//Stop clock
	double duration = (clock() - startTime) / (double) CLOCKS_PER_SEC;
	printf("Time: %f seconds\n", duration);	

	//Cleanup
	delete[] board_p1s;
	delete[] board_p2s;
	delete[] scores;
	return;
	
	*/
	//CPU
	Board board;
	board.print();
	//Start clock
	srand((unsigned int) time(0));
	clock_t startTime = clock();	

	Board move = getMoveMonteCarlo(board);
	int pos, quad, rot;
	board.deriveMove(move, pos, quad, rot);
	move.print();
	//Output
	printf("{\"pos\":%i, \"quad\":%i, \"rot\":%i}\n", pos, quad, rot);	
	

	//Stop clock
	double duration = (clock() - startTime) / (double) CLOCKS_PER_SEC;
	printf("Time: %f seconds\n", duration);	
}