//Constants
#define ALL_ROTATIONS 8
#define BOARD_QUADS 4
#define BOARD_SPACES 36
#define COL_SPACES 6
#define INVALID -1
#define NUM_TO_WIN 5
#define QUAD_SPACES 9
#define QUAD_ROW_SPACES 3
#define QUAD_COL_SPACES 3
#define QUAD_HALF_COUNT 2
#define ROW_SPACES 6
#define SIMULATIONS 1000000

//Enums
enum {PLAYER1, PLAYER2};
enum {ROT_CLOCKWISE, ROT_ANTICLOCKWISE};

//Index maps
constant int ROW[] = {0,0,0,1,2,2,2,1,1,0,0,0,1,2,2,2,1,1,3,3,3,4,5,5,5,4,4,3,3,3,4,5,5,5,4,4};
constant int COL[] = {0,1,2,2,2,1,0,0,1,3,4,5,5,5,4,3,3,4,0,1,2,2,2,1,0,0,1,3,4,5,5,5,4,3,3,4};
constant int POS[ROW_SPACES][COL_SPACES] = {
	{0,1,2,9,10,11},        //Row 0
	{7,8,3,16,17,12},       //Row 1
	{6,5,4,15,14,13},       //Row 2
	{18,19,20,27,28,29},    //Row 3
	{25,26,21,34,35,30},    //Row 4
	{24,23,22,33,32,31}     //Row 5
};

//Masks
#define bitCount(board) popcount(board)
constant ulong FULL = 0xfffffffff;
constant ulong INITIAL = 0x0;
constant ulong QUADS[] = {0xff,0x1fe00,0x3fc0000,0x7f8000000}; //Doesn't include centers
constant ulong WINS[] = {
	0x607, 0xe06, 0x30188, 0x31108, 0xc070, 0xe030, 0x181c0000, 0x38180000, 0xc06200000, 0xc44200000, 0x301c00000, 0x380c00000, //Horizontal
    0x20400c1, 0x30400c0, 0x4080122, 0x4880120, 0x30001c, 0x700018, 0x408018200, 0x608018000, 0x810024400, 0x910024000, 0x60003800, 0xe0003000, //Vertical    
    0x5000800a, 0x808000111, 0x888000110, 0x5001000a0, //Diagonal top-left to bottom-right
    0x2090410, 0x4128800, 0x5128000, 0x8a05000 //Diagonal top-right to bottom-left
};

//Macros
inline ulong POS_TO_MPOS(int pos) { 
	return 1L << pos; 
}
inline int MPOS_TO_POS(ulong mpos) {
	//ulong and double (cl_khr_fp64) not supported on some Intel GPU's
	//so divide mpos into lo and hi uint to use log2
	uint lo = log2((float)mpos);
	uint hi = (uint)(mpos >> 32);
	if (hi == 0) return lo;
	else return	log2((float)hi) + 32 + lo;
}
inline ulong RotL(ulong num, int pos) { //This rotates only the first 8 bits
	return ((num << pos) % 0x100) | (num >> (8 - pos));
}
inline ulong RotR(ulong num, int pos) {
	return (num >> pos) | ((num << (8 - pos)) % 0x100); //This rotates only the first 8 bits
}
inline ulong Not(ulong x) {
	return (~x) & FULL;	                
}

inline bool bitScanNext(ulong* board, int* pos) {   
   if (*board == 0) return false;
   *pos = popcount((*board & -(*board)) - 1);
   *board ^= POS_TO_MPOS(*pos);
   return true;
}
//Java random - because OpenCL has no built in rand function
inline uint rand (ulong* seed) {
	*seed = ((*seed) * 0x5DEECE66DL + 0xBL) & ((1L << 48) - 1);
	return (*seed >> 16);
}


//Board object
struct Board {
	ulong p1;
	ulong p2;
    bool turn;	
};

void Board_new(struct Board* b) {
    b->p1 = INITIAL;
    b->p2 = INITIAL;
    b->turn = PLAYER1;		
}


ulong Board_placePin(struct Board* b, int pos) {	    
    ulong avail = Not(b->p1 | b->p2);
    ulong mpos = POS_TO_MPOS(pos);	
	
    if (avail & mpos) {                
        if (b->turn == PLAYER1) b->p1 = b->p1 ^ mpos;        
        else b->p2 = b->p2 ^ mpos; //Player 2				
        return true;
    }	
    else return false;
}

ulong Board_rotateQuad(ulong board, int quad, int rot) {      

	//Extract quad from board  
	ulong quadUnshifted = board & QUADS[quad];
	ulong quadBoard = quadUnshifted >> (quad * QUAD_SPACES); 

	//Bitwise rotate, 3 places will rotate 90 degrees - note bitwise rot is opposite direction of visual
	ulong rotQuad = (rot == ROT_CLOCKWISE)? RotL(quadBoard, QUAD_HALF_COUNT) : RotR(quadBoard, QUAD_HALF_COUNT);    

	//Add the rotated quad back to the board
	ulong quadShifted = rotQuad << (quad * QUAD_SPACES);
	board = board & Not(QUADS[quad]); //Empty quad    
	ulong rotBoard = board ^ quadShifted;
	return rotBoard;    
}

void Board_rotate(struct Board* b, int quad, int rot) {
    b->p1 = Board_rotateQuad(b->p1, quad, rot);
    b->p2 = Board_rotateQuad(b->p2, quad, rot);	
    b->turn = !b->turn;    
}


void Board_makeRandomMove(struct Board* b, ulong* seed) {
	//Place pin in random open space
    ulong avail = Not(b->p1 | b->p2);
    int pos;
    int availBits[BOARD_SPACES]; 	
    int i = 0;
    while (bitScanNext(&avail, &pos)) {
        availBits[i++] = pos;
    }
    		
	int randPos = availBits[rand(seed) % i];
	if (b->turn == PLAYER1) b->p1 = b->p1 ^ POS_TO_MPOS(randPos);
	else b->p2 = b->p2 ^ POS_TO_MPOS(randPos);
	
	//Rotate
	int randQuad = rand(seed) % BOARD_QUADS;
	int randRot = rand(seed) % 2;
	Board_rotate(b, randQuad, randRot);	
}

bool Board_isWin(struct Board* b) {    
	//Check if there are enough pins on the board for a win   
	ulong board = (b->turn == PLAYER1)? b->p1 : b->p2;
	int count = bitCount(board);
	if (count < NUM_TO_WIN) return false;     

	for (int i = 0; i < 32; i++) { //There are 32 possible winning lines of 5
		ulong win = WINS[i];
		if ((board & win) == win) return true;			
	}
	return false;
}

int simulate(struct Board b, ulong* seed) {		
	bool curPlayer = b.turn;
	int moveCount = bitCount(b.p1 | b.p2);
	int i;	
	for (i = 0; i < (BOARD_SPACES - moveCount); i++) {
		Board_makeRandomMove(&b, seed); 
		
		if (Board_isWin(&b)) {
			if (b.turn == curPlayer) return 1; //Win
			else return -1; //Loss
		}
	}
	return 0; //Tie
}


void kernel evalMove(global const ulong* board_p1s, global const ulong* board_p2s, int turn, int initSeed, global int* scores) {	
	int id = get_global_id(0);
	ulong randSeed = initSeed + id;
	struct Board board;
	Board_new(&board);	
	board.p1 = board_p1s[id];
	board.p2 = board_p2s[id];
	board.turn = turn;
	
	int score = 0;
	for (int i = 0; i < SIMULATIONS; i++) {
		score += simulate(board, &randSeed);
	}
	
	scores[id] = score;	
	
}  